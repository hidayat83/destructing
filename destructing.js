// Belajar Destructuring

// contoh destructur array
const listMobil = ["jeruk", "salak", "mangga", "anggur", "durian"];
const mobil1 = listMobil[0];
const mobil2 = listMobil[1];
const mobil3 = listMobil[2];
console.log(mobil1, mobil2, mobil3);

// Destructur
const [pertama, kedua, ketiga, ...lainnya] = listMobil;
console.log(pertama, kedua, ketiga, lainnya);

// contoh destructur object
const mobil = {
  merek: "Toyota",
  harga: 250000,
  Keluaran: 2022,
  bensin : "solar",
  Spesifikasi: {
    Type: "Toyota Alphard",
    warna:"Hitam"
  }
};

const merekMobil = mobil.merek;
const hargaMobil = mobil.harga;
const spekMobil = mobil.Spesifikasi.Type;
console.log(merekMobil,hargaMobil,spekMobil);

// destructur
const {
  merek,
  harga,
  Spesifikasi: { Type },
  ...dataLainya
} = mobil;
console.log(merek, harga, Type, dataLainya);

// contoh destructur di function parameter
function penjumlahan([angka1, angka2]) {
  const penjumlahan = angka1 + angka2;
  const pesan = `hasil penjulahan kedua bilangan adalah ${penjumlahan}`;
  console.log(pesan);
}

function printMobil({ merek, harga, Spesifikasi: { Type } }) {
  const pesan = `Buku ${mobil} type ${Type} dijual dengan harga ${harga}`;
  console.log(pesan);
}
//Default value/alias
function printMobil2({ merek, harga:hrg, velk = "Racing" }){
	console.log(merek,hrg ,velk)
}

printMobil(mobil);
penjumlahan([10, 5]);
printMobil2(mobil);